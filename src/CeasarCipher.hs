module CeasarCipher where

import qualified RIO.Text as T
import qualified RIO.Vector.Boxed as V
import Data.Char (isAscii, isControl)

-- | Printable Ascii characters.
type PrAscii = Char

-- | Reference for printable ascii characters. Same as @V.slice 32 95 $
-- V.fromList [minBound :: Char..maxBound]@. This is not used except in
-- testing, since the range of printable ascii is known.
ascii :: V.Vector Char
ascii = V.fromList $ [c | c <- [minBound..maxBound], isAscii c, not $ isControl c]

-- | Index of lower bound of printable ascii in @[minBound :: Char .. maxBound]@.
minRangeAscii :: Int
minRangeAscii = 32

-- | Index of upper bound of printable ascii in @[minBound :: Char .. maxBound]@.
maxRangeAscii :: Int
maxRangeAscii = 127

-- | Number of printable ascii characters.
lengthAscii :: Int
lengthAscii = maxRangeAscii - minRangeAscii

-- | Return @True@ if @c@ is in the printable ascii range.
isPrAscii :: Char -> Bool
isPrAscii c = i >= minRangeAscii && i <= maxRangeAscii where i = fromEnum c

-- | Like @toEnum@, but specialized to @PrAscii@ and avoiding a runtime
-- exception by considering the printable ascii integer range as a circle.
--
-- >>> import Data.Char (isAscii, isControl)
-- >>> map intToAscii [0..lengthAscii - 1] == [PrAscii c | c <- [minBound..maxBound], isAscii c, not $ isControl c]
-- >>> intToAscii lengthAscii == intToAscii 0
-- >>> intToAscii (-1) == intToAscii (lengthAscii - 1)
intToAscii :: Int -> PrAscii
intToAscii = toEnum . (minRangeAscii +) . (`mod` lengthAscii)

-- | @fromEnum@ specialized to @PrAscii@, resetting the index to @0@. For
-- example, @fromEnum ' ' == 32@ and @intFromAscii $ PrAscii ' ' == 0@.
--
-- >>> import Data.Char (isAscii, isControl)
-- >>> map intFromAscii [PrAscii c | c <- [minBound..maxBound], isAscii c, not $ isControl c] == [0..lengthAscii - 1]
intFromAscii :: PrAscii -> Int
intFromAscii c = (fromEnum c - minRangeAscii) `mod` lengthAscii

-- | Type alias for @T.Text@ intended only to hold printable ascii characters.
-- At the moment, this is only checked in the @CLI.CeasarCipherApp@ on
-- construction, using utilities from this module.
type PrAsciiText = T.Text

-- | Construct @PrAsciiText@, returning an error at the first character that is
-- not in the printable ascii range.
prAsciiText :: T.Text -> Either String PrAsciiText
prAsciiText = fmap T.pack . traverse chk . T.unpack
  where chk c 
          | not $ isPrAscii c = Left $ c : " is not a printable ascii character."
          | otherwise = Right c

-- | Newtype wrappers for encoded or decoded text. 
newtype PlainText = PlainText { pText :: PrAsciiText } deriving (Show, Eq)
newtype CipherText = CipherText { cText :: PrAsciiText } deriving (Show, Eq)

-- | Rotate @c@ by @k@ positions, treating @PrAscii@ as an integer circle.
rotChar :: Int -> PrAscii -> PrAscii
rotChar k = intToAscii . (k +) . intFromAscii

-- | Encode @PlainText@ using the given @key@, by rotating each @Char@ the
-- number of positions given by @key@. @encrypt k@ is the inverse of @decrypt k@
-- for a given @k@.
encrypt :: Int -> PlainText -> CipherText
encrypt key = CipherText . T.map (rotChar key) . pText

-- | Decode @CipherText@ using the given @key@, by rotating in the reverse
-- direction each @Char@ the number of positions given by @key@. @encrypt k@ is
-- the inverse of @decrypt k@ for a given @k@.
decrypt :: Int -> CipherText -> PlainText
decrypt key = PlainText . T.map (rotChar (-key)) . cText
