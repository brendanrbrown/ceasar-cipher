module CLI where

import           Options.Applicative
import RIO
import qualified RIO.Text as T
import CeasarCipher

  {- Argument parsers -}
data Command = Encrypt | Decrypt deriving (Show, Eq)

data Args = Args { cmd :: Command, key :: Int, msg :: Text }

parseCommand :: Parser Command
parseCommand = flag' Encrypt (long "encrypt" <> short 'e') <|> flag' Decrypt (long "decrypt" <> short 'd')

parseArgs :: Parser Args
parseArgs = Args <$> parseCommand
  <*> option auto
    ( long "key" <> short 'k' <> help "Key for encrypting / decrypting" <> metavar "INT" )
  <*> strArgument (metavar "MESSAGE")

cliArgs :: ParserInfo Args
cliArgs = info (parseArgs <**> helper) (header "Ceasar's cipher")

  {- Application -}
type CeasarCipherApp = RIO Args

data CeasarCipherError = InvalidChar String | Unhandled String deriving (Show, Eq)

instance Exception CeasarCipherError

runCipher :: Args -> CeasarCipherApp a -> IO a
runCipher = runRIO

-- | Convert @msg@ to @PrAsciiText@, at present an alias for @Text@, throwing a
-- runtime exception if a character is not a printable ascii character.
msgToPrAscii :: CeasarCipherApp PrAsciiText
msgToPrAscii = do
  m <- asks msg

  case prAsciiText m of
    Left err -> throwIO $ InvalidChar err
    Right txt -> pure txt


cipherOp :: PrAsciiText -> CeasarCipherApp T.Text
cipherOp t = do
  arg <- ask
  case cmd arg of
    Encrypt -> pure $ cText $ encrypt (key arg) (PlainText t)
    Decrypt -> pure $ pText $ decrypt (key arg) (CipherText t)

ceasarCipherApp :: CeasarCipherApp ()
ceasarCipherApp = msgToPrAscii >>= cipherOp >>= (liftIO . putStrLn . T.unpack)
