A simple [substitution
cipher](https://en.wikipedia.org/wiki/Substitution_cipher) on the [printable
ascii characters](https://en.wikipedia.org/wiki/ASCII#Printable_characters).
Notably, those do not include the newline character '\n'.

The printable ascii range is treated as an integer circle on Haskell's `Int`
type, allowing for any key in the range `[-2^29 .. 2^29 - 1]`.

## Usage
Best used to amuse children, or to bother them with arthimetic problems giving
the key to decode the ciphertext.

```
ceasar-cipher --encode --key 1 "I'll pay you 10 bucks for that llama!"
```

prints 

```
J(mm!qbz!zpv!21!cvdlt!gps!uibu!mmbnb"
```

Remember to escape or protect your characters in some way. This is particularly annoying when using this for people who like multiple exclamation points!!

In the bash shell:

```
ceasar-cipher --decode --key 1 'J(mm!qbz!zpv!21!cvdlt!gps!uibu!mmbnb"'
```

See `ceasar-cipher --help`.
